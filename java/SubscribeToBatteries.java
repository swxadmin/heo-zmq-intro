package org.sofwerx.subscribetobatteries;

import org.json.JSONObject;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class SubscribeToBatteries {

    private static final int PORT_NUM = 5588;
    private static final String SERVER_ADDRESS = "3.220.173.248";
    private static final String SUB_TOPIC = "Sim_Battery_Info";

    public static void main (String [] args) {
        try (ZContext context = new ZContext()) {
            // Socket to connect to server
            ZMQ.Socket socket = context.createSocket(SocketType.SUB);
            socket.connect("tcp://" + SERVER_ADDRESS + ":" + PORT_NUM);

            // Subscribe to topic
            socket.subscribe(SUB_TOPIC.getBytes(ZMQ.CHARSET));

            // Grab 20 updates and exit
            for (int i = 0; i < 20; i++) {
                // Publisher sends two messages first is publish topic second is publish data
                String publishedTopic = socket.recvStr();

                // Make sure publish data is present prior to calling recv a second time
                if (socket.hasReceiveMore()) {
                    String topicData = socket.recvStr();

                    // Print out the topic title
                    System.out.println("Topic: " + publishedTopic);
                    try {
                        // Attempt to load topicData into JSONObject and then print received JSON
                        JSONObject pubData = new JSONObject(topicData);
                        System.out.println("Data:\n" + pubData.toString(4));
                    } catch (Exception ex) {
                        System.out.println("An exception occurred while trying to load JSON: " + ex.toString());
                    }
                }
            }
            socket.close();
        }
    }
}
