package org.sofwerx.requestvideostreams;

import org.json.JSONObject;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class RequestVideoStreams {

    private final static int PORT_NUM = 5555;
    private final static String REQUEST_VIDEOS = "getVideos";
    private final static String SERVER_ADDRESS = "3.220.173.248";

    public static void main (String[] args) {
        try (ZContext context = new ZContext()) {
            // Socket to connect to server
            ZMQ.Socket socket = context.createSocket(SocketType.REQ);
            socket.connect("tcp://" + SERVER_ADDRESS + ":" + PORT_NUM);

            // Request a list of available videos
            System.out.println("Requesting a list of available videos");
            socket.send(REQUEST_VIDEOS);

            // Get response from server and put in JSON object
            String respString = socket.recvStr();

            // Attempt to load received JSON and then print it
            try {
                JSONObject vidsObj = new JSONObject(respString);
                System.out.println("Response from server:");
                System.out.println(vidsObj.toString(4));
            } catch (Exception ex) {
                System.out.println("An exception occurred while trying to load JSON: " + ex.toString());
                System.out.println("Server Response: \n" + respString);
            }

            socket.close();
        }
    }
}
