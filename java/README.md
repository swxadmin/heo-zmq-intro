# Java Examples
Java examples for ZMQ Request-Reply and Publish-Subscribe

## Dependencies
The examples in this directory require ZMQ and JSON libraries.

### Maven
If using Maven add the following to the dependencies section:

```xml
<dependency>
    <groupId>org.zeromq</groupId>
    <artifactId>jeromq</artifactId>
    <version>0.5.2</version>
</dependency>
<dependency>
    <groupId>org.json</groupId>
    <artifactId>json</artifactId>
    <version>20190722</version>
</dependency>
```

### Gradle
If using Gradle add the following lines to dependencies section of the gradle file:

```
compile group: 'org.zeromq', name: 'jeromq', version: '0.5.2'
compile group: 'org.json', name: 'json', version: '20190722'
```
