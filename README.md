# HEO ZMQ Introduction 
This repository is meant to help introduce new users to ZMQ which the HEO project utilizes.

The java and python folders contains examples for Request-Reply and Publish-Subscribe.

### Link to ZMQ's official guide and examples
http://zguide.zeromq.org
