#!/usr/bin/python3

"""
Title: ZeroMQWrapper.py
Description:    This library will contain API functions for sending and
                receiving ZeroMQ messages
"""

import threading
import zmq

# Decorator for simplifying a threaded function:
# https://stackoverflow.com/questions/19846332/python-threading-inside-a-class/19846691
def threaded (func):
    def wrapper (*args, **kwargs):
        thread = threading.Thread(target=func, args=args, kwargs=kwargs)
        # thread.daemon = True
        thread.start()
        return thread
    return wrapper

class MessageHandler:

    MSG_TYPE_REQREP = 'REQ/REP'
    MSG_TYPE_PUBSUB = 'PUB/SUB'

    # Initialization - Handles creation, context setup and port binding
    def __init__(self, ip='127.0.0.1', port=6666, server=False, recv_hndlr=None, 
        msg_type=MSG_TYPE_REQREP, msg_topic=b"", multicast=False, multicast_interface='eth0'):

        # Set Variables
        self.__ip = ip
        self.__port = str(port)
        self.__recv_hndlr = recv_hndlr
        self.__msg_type = msg_type
        self.__msg_topic = msg_topic
        self.__recv_thread_hndlr = None
        self.__mutex = threading.Lock()
        self.__trigger = threading.Event()
        self.__resp_expected = False
        self.__poll = None
        self.__request = None
        self.__transport_proto = None

        # Create context
        self.__context = zmq.Context.instance()

        # Set transport protocol
        if (multicast):
            self.__transport_proto = 'epgm://' + multicast_interface + ';'
        else:
            self.__transport_proto = 'tcp://'

        # Determine if this instance is a Server or a Client
        self.__server = server
        if server:
            
            # Create socket and set type appropriately, then bind
            if (msg_type == MessageHandler.MSG_TYPE_REQREP):
                
                self.__socket = self.__context.socket(zmq.REP)
                
                # Set Receive Thread Handler
                self.__thread_hndl = self.receive()
                
                # Trigger receive thread
                self.__trigger.set()
            elif (msg_type == MessageHandler.MSG_TYPE_PUBSUB):
                
                self.__socket = self.__context.socket(zmq.PUB)
            
            # Update MSG Rate
            self.__socket.setsockopt(zmq.RATE, 8000)
            # Bind on socket - server side
            self.__socket.bind(self.__transport_proto + ip + ":" + str(port))

        else:
            
            # Create socket and set type appropriately, then connect
            if (msg_type == MessageHandler.MSG_TYPE_REQREP):
               
                # Create socket and set type to REQ
                self.__socket = self.__context.socket(zmq.REQ)

                # Register a poller
                self.__poll = zmq.Poller()
                self.__poll.register(self.__socket, zmq.POLLIN)

                # Set receive thread handler
                self.__thread_hndl = self.receive_reply()

            elif (msg_type == MessageHandler.MSG_TYPE_PUBSUB):

                # Create socket and set type to SUB
                self.__socket = self.__context.socket(zmq.SUB)

                # Set Subscribe topic
                self.__socket.setsockopt(zmq.SUBSCRIBE, msg_topic)

                # Set Receive Thread Handler
                self.__thread_hndl = self.receive()
            # Update EPGM Rate
            self.__socket.setsockopt(zmq.RATE, 8000)
            # Connect on socket - client side
            self.__socket.connect(self.__transport_proto + ip + ":" + str(port))

    # Reconnect function to be used by a client
    def reconnect(self):
        # Guarantee we are not a server
        if self.__server == False:
             
            # Close the socket
            self.__socket.setsockopt(zmq.LINGER, 0)
            self.__socket.close()

            # Close the Poller
            self.__poll.unregister(self.__socket)

            # Update retry count

            # Reopen connection
            self.__socket = self.__context.socket(zmq.REQ)
            self.__socket.connect(self.__transport_proto + self.__ip + ":" + self.__port)

            # Register a poller
            self.__poll = zmq.Poller()
            self.__poll.register(self.__socket, zmq.POLLIN)

    # Manages the reception of messages asynchronously and dispatches message to receive handler
    @threaded
    def receive_reply(self):
        # Run indefinitely (May change to retry limit here)
        while True:
            # Wait until we are anticipating a response
            self.__trigger.wait()

            retry = True
            while retry == True:
                # Poll
                events = dict(self.__poll.poll(1500))

                # Checked poll and something was received
                if events.get(self.__socket) == zmq.POLLIN:
                    
                    # Get reply
                    reply = self.__socket.recv_multipart()
            
                    # Check reply
                    if not reply[1]:
                        # Empty frame - do nothing
                        break
                    else:
                        # Call handler with received reply
                        self.__recv_hndlr(reply[0], reply[1])

                        # End retry
                        retry = False

                # Checked poll and nothing received
                else:
                    print ("Error: Timed out. Resetting Connection to Server")

                    # Reset connection
                    self.reconnect()

                    # Resend connection
                    self.__socket.send_multipart([self.__msg_topic, self.__request], zmq.NOBLOCK)

            # Re-lock trigger to be unlocked later
            self.__trigger.clear()

    # Manages the reception of messages asynchronously and dispatches message to receive handler with no poll or reconnect
    @threaded
    def receive (self):
        # Run indefinitely (May change to retry limit here)
        while True:
            if (self.__msg_type != MessageHandler.MSG_TYPE_PUBSUB):
                # Wait until we are anticipating a response
                self.__trigger.wait()
                  
            # Get message
            message = self.__socket.recv_multipart()
    
            # Check reply
            if not message[1]:
                # Empty frame - do nothing
                break
            else:
                if (self.__msg_type != MessageHandler.MSG_TYPE_PUBSUB):
                    # Re-lock trigger to be unlocked later
                    self.__trigger.clear()

                # Call handler with received reply
                self.__recv_hndlr(message[0], message[1])

            

    def send(self, message):
        # Ignore if we are a client subscriber as pub/sub
        if (self.__msg_type == MessageHandler.MSG_TYPE_PUBSUB):
            if (not self.__server):
                return
        
        # Story message in case we need to re-send
        self.__request = message

        # Send message
        self.__socket.send_multipart([self.__msg_topic, message], zmq.NOBLOCK)

        # Trigger receive thread if not pub/sub
        if (self.__msg_type != MessageHandler.MSG_TYPE_PUBSUB):
            self.__trigger.set()

    def default_handler(self):
        return

    def __del__(self):
        self.__thread_hndl.join()

        
