from ZeroMQWrapper import MessageHandler
import json
import time

portNum = 5588
serverAddress = "3.220.173.248"

received_update = False
update = ""

# Handler for received messages
def msgHandler(topic, message):
    global update
    update = message
    global received_update
    received_update = True

# Initialize client
client = MessageHandler(ip=serverAddress, port=portNum, recv_hndlr=msgHandler, msg_type=MessageHandler.MSG_TYPE_PUBSUB, msg_topic="Sim_Battery_Info")

# Grab 20 updates and exit
for i in range(20):
    try:
        # Wait for response from server
        while not received_update:
            time.sleep(0.5)
        # Attempt to load JSON and print out received data
        battery_status = json.loads(update)
        print(update)
        received_update = False
    except Exception as ex:
        print "An exception occurred while trying to load JSON: " + str(ex) + "\n"

del client
