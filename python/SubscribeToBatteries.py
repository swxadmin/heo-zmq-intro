import json
import zmq

portNum = 5588
serverAddress = "3.220.173.248"
subTopic = "Sim_Battery_Info"

# Setup context and socket
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://" + serverAddress + ":" + str(portNum))

# Subscribe to the desired topic
socket.setsockopt(zmq.SUBSCRIBE, subTopic)

# Grab 20 updates and exit
for i in range(20):
    try:
        # Receive data from server.
        publishedData = socket.recv_multipart() 

        if (len(publishedData) == 2):
            # Attempt to load JSON and print out received data
            data = json.loads(publishedData[1])
            print "Received data from publisher"
            print "\tTopic: " + publishedData[0]
            print "\tData: " + str(data) + "\n"
    except Exception as ex:
        print "An exception occurred while trying to load JSON: " + str(ex) + "\n"

socket.close()
context.term()
