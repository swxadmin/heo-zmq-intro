# Python Examples
Python examples for ZMQ Request-Reply and Publish-Subscribe

## Dependencies
The additional dependency required for examples in this directory is the ZMQ python library. The python-zmq library can be installed using the built in linux package management system or via pip: 
### Ubuntu
```
sudo apt-get install python-zmq
```
### Fedora
```
sudo yum install python-zmq
```

### pip
```
pip install pyzmq
```
