from ZeroMQWrapper import MessageHandler
import json
import time

portNum = 5555
requestVideos = "getVideos"
serverAddress = "3.220.173.248"

received_reply = False
response = ""

# Handler for received messages
def msgHandler(topic, message):
    global response
    response = message
    global received_reply
    received_reply = True

# Initialize client
client = MessageHandler(ip=serverAddress, port=portNum, recv_hndlr=msgHandler)

# Send request for videos
client.send(requestVideos)

# Wait for response from server
while not received_reply:
    time.sleep(0.5)

try:
    # Load the response into JSON object and then print it
    videos = json.loads(response)
    print videos
except Exception as ex:
    print "An exception occurred while trying to load JSON: " + str(ex) + "\n"
    print "Server's Response: \n" + response

del client
