import json
import zmq

portNum = 5555
requestVideos = "getVideos"
serverAddress = "3.220.173.248"

# Setup context and socket
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://" + serverAddress + ":" + str(portNum))

# Request a list of available videos
socket.send(requestVideos)

# Get response from server
response = socket.recv()

try:
    # Load the response into JSON object and then print it
    videos = json.loads(response)
    print videos 
    print "\n"

except Exception as ex:
    print "An exception occurred while trying to load JSON: " + str(ex) + "\n"
    print "Server's Response: \n" + response

socket.close()
context.term()
